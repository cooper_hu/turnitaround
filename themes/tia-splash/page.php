<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tia-splash
 */

get_header(); ?>

	<img src="<?= get_template_directory_uri(); ?>/logo-turn-it-around.svg"/>

<?php get_footer();
