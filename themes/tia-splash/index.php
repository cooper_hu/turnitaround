<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tia-splash
 */

get_header();
?>

	<img src="<?= get_template_directory_uri(); ?>/logo-turn-it-around.svg"/>

<?php
get_sidebar();
get_footer();
