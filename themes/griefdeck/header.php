<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grief_Deck
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <?php /*
    <?php if ( $_SERVER['SERVER_NAME'] == 'griefdeck.com' ) : ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-Q0WNQ1KYGB"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-Q0WNQ1KYGB');
        </script>
    <?php else : ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-V0JXGK95CS"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-V0JXGK95CS');
        </script>
    <?php endif; ?>
    */ ?>

    <style>
        html.sr .grid-card {
            visibility: hidden; 
        }
    </style>

    <script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>
    <script src="<?= get_template_directory_uri(); ?>/assets/js/jquery-3.5.1.min.js"></script>

    <script>
        var slideUp = {
            interval: 100,
            delay: 100,
            duration: 1000,
            distance: '40px',
            origin: 'bottom',
        };

        ScrollReveal().reveal('.grid-card', slideUp);
    </script>

    
	
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/magnific-popup.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/selectric.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/photoswipe.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/photoswipe-default-skin.css"/>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css?v=2"/>

	<?php wp_head(); ?>
</head>

<?php 

$pageTheme = get_field('theme_color'); 

if (ICL_LANGUAGE_CODE) {
    if( ICL_LANGUAGE_CODE == 'es' ) {
        $homeURL = '/?lang=es';
        $searchTxt = 'Buscar';

    } else if( ICL_LANGUAGE_CODE == 'fr' ) {
        $homeURL = '/?lang=fr';
        // $homeTitle = 'Consolarte';
        $searchTxt = 'Chercher';
    } else {
        $homeURL = '/';
        $searchTxt = 'Search';
    }
} else {
    $homeURL = '/';
    $searchTxt = 'Search';
} ?>

<body <?php if ($pageTheme) { body_class($pageTheme); } else { body_class(); }  ?>>
<?php wp_body_open(); ?>
	
	<div class="navbar">
		<div class="navbar__logo">  
            <a href="<?= $homeURL; ?>">
                <img class="home-logo" src="<?= get_template_directory_uri(); ?>/assets/images/logo-turn-it-around.svg"/></a>
            <?php /*
            <?php if (ICL_LANGUAGE_CODE) : ?>
                <?php if( ICL_LANGUAGE_CODE == 'es' ) : ?>
                    <a href="<?= $homeURL; ?>"><?= $homeTitle; ?><br/><span>The Artists&#146; Grief Deck en Español</span></a>
                <?php else : ?> 
    		      <a href="<?= $homeURL; ?>"><?= $homeTitle; ?></a>
                <?php endif; ?>
            <?php else : ?>
                <a href="<?= $homeURL; ?>"><?= $homeTitle; ?></a>
            <?php endif; ?>
            */ ?>
		</div>
        <a href="#" class="navbar__toggle">
            <span></span>
            <span></span>
            <span></span>
        </a>
		<nav class="navbar__nav">
			<?php wp_nav_menu(array(
                'theme_location' => 'main_menu',
                'menu_class' => 'navbar-list'
			)); ?>
                    
            <div class="navbar__search-form">
                <a href="#" class="navbar__search-form--btn">Open Search</a>
    			<form role="search" method="get" class="navbar__search-form--form" action="/">
                    <label>
                        <input type="text" class="search-field" placeholder="<?= $searchTxt; ?>..." value="" name="s">
                        <input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
                    </label>
                    <input type="submit" class="search-submit hide-desktop" value="Search">
                </form>
            </div>
		</nav>
	</div>

    <nav class="navbar-drawer">
        <a href="#" class="navbar-drawer__close-btn"></a>
        <?php wp_nav_menu(array(
            'theme_location' => 'main_menu',
            'menu_class' => 'navbar-list'
        )); ?>
        <form role="search" method="get" action="/" class="navbar-drawer__search-form">
            <label>
                <input type="text" placeholder="<?= $searchTxt; ?>..." value="" name="s">
                <input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
            </label>
            <input type="submit" class="search-submit" value="Search">
        </form>
    </nav>
    <div class="navbar-overlay"></div>
<?php /*
<!-- #site-navigation -->
*/ ?>