<?php
/**
 * Grief Deck functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Grief_Deck
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'griefdeck_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function griefdeck_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Grief Deck, use a find and replace
		 * to change 'griefdeck' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'griefdeck', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
            'main_menu'    => __('Main Menu', 'text_domain'),
			'footer_menu'  => __('Footer Menu', 'text_domain'),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'griefdeck_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'griefdeck_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function griefdeck_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'griefdeck_content_width', 1000 );
}
add_action( 'after_setup_theme', 'griefdeck_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function griefdeck_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'griefdeck' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'griefdeck' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'griefdeck_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function griefdeck_scripts() {
	wp_enqueue_style( 'griefdeck-style', get_stylesheet_uri(), array(), _S_VERSION );

	// wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/map-place-archive.js', array(), '1.0', true );
	wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.5.1.min.js', array(), '3.5.1', true);
	wp_enqueue_script( 'js-bundle', get_template_directory_uri() . '/assets/js/js-bundle.js', array('jquery'), '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'griefdeck_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Hides media button from Card CPT WYSIWYG EDITOR
function check_post_type_and_remove_media_buttons() {
	global $current_screen;
        // use 'post', 'page' or 'custom-post-type-name'
	if( 'card' == $current_screen->post_type ) {
		// add_action( 'media_buttons_context' , create_function('', 'return;') );
		remove_action( 'media_buttons', 'media_buttons' );
	}
}
add_action('admin_head','check_post_type_and_remove_media_buttons');


// Remove items from WP Dashboard
function remove_menu_items() {
	// Hides default "Post" from Wordpress Dashboard
    remove_menu_page('edit.php');
    // Hides comments section from Dashboard
    remove_menu_page( 'edit-comments.php' );
}
add_action('admin_menu', 'remove_menu_items');

// Remove items from Admin Bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('new-post');
    $wp_admin_bar->remove_menu('new-user');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );

// Move Yoast Metabox to bottom of page by default
function wpcover_move_yoast() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'wpcover_move_yoast');

// Setup Custom Post Types
// Register Custom Post Type
function card_cpt() {
	$labels = array(
		'name'                  => _x( 'Cards', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Card', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Cards', 'text_domain' ),
		'name_admin_bar'        => __( 'Card', 'text_domain' ),
		'archives'              => __( 'Card Archives', 'text_domain' ),
		'attributes'            => __( 'Card Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Card:', 'text_domain' ),
		'all_items'             => __( 'All Cards', 'text_domain' ),
		'add_new_item'          => __( 'Add New Card', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Card', 'text_domain' ),
		'edit_item'             => __( 'Edit Card', 'text_domain' ),
		'update_item'           => __( 'Update Card', 'text_domain' ),
		'view_item'             => __( 'View Card', 'text_domain' ),
		'view_items'            => __( 'View Cards', 'text_domain' ),
		'search_items'          => __( 'Search Card', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this card', 'text_domain' ),
		'items_list'            => __( 'Cards list', 'text_domain' ),
		'items_list_navigation' => __( 'Cards list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter cards list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Card', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-images-alt2',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'cards',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'card', $args );
}
add_action( 'init', 'card_cpt', 0 );

// Build Custom toolbars
// Create custom toolbars
function my_toolbars( $toolbars ) {

    $toolbars['List' ] = array();
    $toolbars['List' ][1] = array('bullist', 'numlist', 'undo', 'redo', 'removeformat', 'fullscreen' );

    $toolbars['Ultra Slim' ] = array();
    $toolbars['Ultra Slim' ][1] = array('bold' , 'italic', 'underline', 'link', 'superscript', 'bullist', 'numlist', 'undo', 'redo', 'removeformat', 'fullscreen' );

    return $toolbars;
}

add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );



// Adds custom post types to the tags archive page.
function wpse28145_add_custom_types( $query ) {
    if( is_tag() && $query->is_main_query() ) {

        // this gets all post types:
        $post_types = get_post_types();

        // alternately, you can add just specific post types using this line instead of the above:
        // $post_types = array( 'post', 'your_custom_type' );

        $query->set( 'post_type', $post_types );
    }
}
add_filter( 'pre_get_posts', 'wpse28145_add_custom_types' );



// Sets height of Resources description ACF WYSIWYG editor 
add_action('acf/input/admin_head', 'my_acf_modify_wysiwyg_height');

function my_acf_modify_wysiwyg_height() {
    
    ?>
    <style type="text/css">
    	#acf-group_5fc84e9e81d1c .mce-edit-area iframe {
            height: initial !important;
        }

        #acf-group_5fc84e9e81d1c .acf-editor-wrap iframe {
        	min-height: 100px !important;
        }
    </style>
    <?php    
    
}

// Adds image caption to post result
add_filter( 'relevanssi_content_to_index', 'rlv_add_attachment_excerpts', 10, 2 );
/**
 * Indexes attachment excerpts for the parent post.
 *
 * This function reads in the attachment excerpts from the database and
 * adds it to the parent post content.
 *
 * @global $wpdb The WordPress database interface.
 *
 * @param string $content The added content.
 * @param object $post    The indexed post object.
 *
 * @return string The added content.
 */
function rlv_add_attachment_excerpts( $content, $post ) {
    global $wpdb;
    $results = $wpdb->get_col(
        $wpdb->prepare(
            "SELECT post_excerpt FROM $wpdb->posts WHERE post_parent = %d",
            $post->ID
        )
    );
    foreach ( $results as $excerpt ) {
        $content .= " $excerpt";
    }
    return $content;
}

// AJAX Loading new content
function load_more_scripts() {
    global $wp_query; 
 
    // In most cases it is already included on the page and this line can be removed
   // wp_enqueue_script('jquery');
 
    // register our main script but do not enqueue it yet
    wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/assets/js/loadmore.js', array('jquery'), '1.0', true );
 	
    // now the most interesting part
    // we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
    // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
    wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
        'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
        'max_page' => $wp_query->max_num_pages
    ) );
 
    wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'load_more_scripts' );

function loadmore_ajax_handler(){

    // $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_type'] = 'card';
    $args['post_status'] = 'publish';
    // $args['post_per_page'] = get_option( 'posts_per_page' );

    /*
    if ($_POST['includeGuide']) {
        $args['post_per_page'] = get_option( 'posts_per_page' ) - 1;
    } else {
        $args['post_per_page'] = get_option( 'posts_per_page' );
    }
    */

    $ppp = get_option( 'posts_per_page' );
    $args['posts_per_page'] = $ppp;

    // Check if guide card is an optiono
    if ($_POST['isFirst']) {
        $frontpage_id = get_option( 'page_on_front' );
        $includeGuideCard = get_field('include_guide_card', $frontpage_id);
        
        if ($includeGuideCard) {
            // echo 'yes: ' . ($ppp - 1);
            $args['posts_per_page'] = ($ppp - 1);
        }
    }

    $args['orderby'] = 'rand';
    
    if ($_POST['excludedRandPosts']) {
    	$args['post__not_in'] = $_POST['excludedRandPosts'];
    }

    if ($_POST['tagID']) {
    	$args['tag_id'] = $_POST['tagID'];
    }

	if ($_POST['searchTerm']) {
		$args['s'] = $_POST['searchTerm'];
    	// echo '<p>SEARCHED: ' . $_GET['s'] . '</p>';
    }

    if ($_POST['catID']) {
        $args['cat'] = $_POST['catID'];

        $category = get_category($_POST['catID']);
        $count = $category->category_count;
        // echo '<p>SEARCHED: ' . $_GET['s'] . '</p>';
    } else {
        // Need count for all posts?
    }

    if ($_POST['lang']) {
        global $sitepress;
        $sitepress->switch_lang($_POST['lang']);
    }

    $the_query = new WP_Query($args);

    if ($_POST['searchTerm']) {
    	relevanssi_do_query($the_query);
    }

    if ($the_query->have_posts()) {

        if ($_POST['isFirst']) {
            if ($includeGuideCard) {
                include get_template_directory() . '/template-parts/guide-card.php';
            }
        }
        
        // var_dump($args);

		while ( $the_query->have_posts() ) {
			$the_query->the_post();
            
            /* if ($_POST['includeGuide'] && $post_count === 1) {
                include get_template_directory() . '/template-parts/guide-card.php';
            } */
            /*
            if ($_POST['catID']) {
                echo $_POST['catID']; 
            }
            */

            // var_dump($args);
            // echo 'PostID: ' . get_the_ID();

            /*
            if ($_POST['excludedPosts']) {
                var_dump($_POST['excludedPosts']);
            }

            if ($_POST['excludedRandPosts']) {
                var_dump($_POST['excludedRandPosts']);
            }
            */
            

			$post_ids[] = get_the_ID(); // Build array of post IDs (to be excluded)

	        include get_template_directory() . '/template-parts/card.php';
	    }
	    echo '<span id="newCardIDs" data-excluded-posts="' . json_encode($post_ids) . '" data-total-posts="' . $count . '"></span>';

        // echo json_encode($post_ids);
    } else {
    	// Echo 'NOPOST' so Javascript can show a different view (JS needs to do this so it can also hide the Load More button (: )
    	echo 'NOPOST';
    }
	wp_reset_postdata();

    die; // here we exit the script and even no wp_reset_query() required!
}
  
add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

// Pass Featured Image information to Card when card is updated
add_action('acf/save_post', 'my_acf_save_post');
function my_acf_save_post( $post_id ) {
	// $imgID = get_field('imgcard', get_post_thumbnail_id()); 
	$imgID = get_post_thumbnail_id(); 
	$artistName = get_field('imgartist', get_post_thumbnail_id()); 
    $artistLocation = get_field('imgartist_location', get_post_thumbnail_id()); 

	// Update with new value.
	update_field('card_artist_name', $artistName);
    update_field('card_artist_location', $artistLocation);
}


function disable_acf_load_field( $field ) {
	$field['disabled'] = 1;
	return $field;
}
add_filter('acf/load_field/name=card_artist_name', 'disable_acf_load_field');
add_filter('acf/load_field/name=card_artist_location', 'disable_acf_load_field');


function namespace_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'nav_menu_item', 'card'
        ));
      return $query;
    }
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );
