<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Grief_Deck
 */

get_header();

if (ICL_LANGUAGE_CODE) {
    if( ICL_LANGUAGE_CODE == 'es' ) {
        $noResult = 'No hay resultados para: ';
        $searchText = 'Buscar: ';
        $cardBtnText = 'Haga clic para continuar';
        $loadBtnText = 'Cargar más tarjetas';
        $btnClass = 'lg';
    } else if( ICL_LANGUAGE_CODE == 'fr' ) {
        $noResult = 'Aucun résultat por: ';
        $searchText = 'Chercher: ';
        $loadBtnText = 'Charger plus de cartes';
        $cardBtnText = 'Cliquez pour continuer';
        $btnClass = 'lg';
    } else {
        $noResult = 'No results for: ';
        $searchText = 'Searched: ';
        $cardBtnText = 'Click to Continue';
        $loadBtnText = 'Load More Cards';
    }
} else {
    $noResult = 'No results for: ';
    $searchText = 'Searched: ';
    $cardBtnText = 'Click to Continue';
    $loadBtnText = 'Load More Cards';
} ?>

<main>
    <?php $ppp = get_option( 'posts_per_page' );

    $searchTerm = $_GET['s'];

    $postsPerPage = $ppp - 1;    

    $args = array(
        // 'post_type' => 'card',
        'posts_per_page' => $postsPerPage,
        's' => $searchTerm,
    ); 

    $the_query = new WP_Query(); 
    
    $the_query->parse_query($args);

    // Pass the query through relevanssi
    relevanssi_do_query( $the_query); ?>

    <?php if ( $the_query->have_posts() ) : ?>

        <div class="grid__container">
            <div class="grid__item">
                <div class="tag-header">
                    <h1><?php printf( esc_html__( $searchText .     '%s', 'griefdeck' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                </div>
            </div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                $post_ids[] = $post->ID; // Build array of post IDs 
            	if (get_post_type() === 'card') : ?>
                	<?php // This markup show match the template (the only difference here is that it needs to open in a new tab.
                    // include get_template_directory() . '/template-parts/card.php'; ?>
                    <?php 
                    $cat = get_the_category();
                    $catID = $cat[0]->term_id;
                    $catColor = get_field('color', $cat[0]); ?>

                    <div class="grid__item">
                        <div class="grid-card">
                            <div class="grid-card__inner">
                                <div class="grid-card__inner--front">
                                    <!-- <span class="grid-card__color-strip" style="background-color: <?= $catColor; ?>"></span> -->
                                    <?= get_the_post_thumbnail( $post_id, 'full' ); ?>
                                </div>

                                <div class="grid-card__inner--back">
                                    <div class="grid-card__content--back" style="background-color: <?= $catColor; ?>">
                                        <div class="grid-card__content--wrapper">
                                            <div>
                                                <h2 class="grid-card__headline"><?php the_title(); ?></h2>
                                                <div class="grid-card__prompt">
                                                
                                                    <?php $promptSummary = get_field('card_summary'); ?>
                                                    <?php if ($promptSummary) : ?>
                                                        <p><?= $promptSummary; ?></p>
                                                    <?php else : ?>
                                                        <?php 

                                                        $contentSample = wpautop(get_the_content()); 
                                                        $contentSample = strip_tags($contentSample);
                                                        $cardSummary = substr($contentSample, 0, 250); 
                                                        if (rtrim($cardSummary) != $cardSummary) : ?>
                                                            <p><?= rtrim($cardSummary); ?>...</p>
                                                        <?php else : ?>
                                                            <p><?= $cardSummary; ?>...</p>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    
                                                    <a class="btn <?= $btnClass; ?>" href="<?php the_permalink(); ?>" target="_blank"><?= $cardBtnText; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else : 
                    // echo '<pre>'; var_dump($post->post_parent); echo '</pre>';  
                    echo '<br/>';
                    echo '<br/>';
                    echo '<br/>';
                    echo '<pre>'; var_dump($post); echo '</pre>'; 

                endif; 
                
            endwhile; ?>
        </div>

        <?php if ( $the_query->found_posts >  $postsPerPage ) : ?>
            <div class="container">
                <div class="grid__load-more">
                    <a href="#" class="btn" id="loadMoreBtn" data-total-posts="<?= $the_query->found_posts; ?>" data-excluded-posts="<?php echo json_encode($post_ids); ?>" data-searchterm="<?= $searchTerm; ?>"><?= $loadBtnText; ?></a>
                </div>
            </div>
        <?php endif; ?>
    <?php else : ?>
    	<div class="grid__container">
            <div class="grid__item double">
                <div class="tag-header">
                    <h1><?php printf( esc_html__( $noResult . '%s', 'griefdeck' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                </div>
                <br/><br/><br/>
            </div>
        </div>
    <?php endif; ?>

       
</main>


<?php get_footer();
