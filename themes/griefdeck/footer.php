<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grief_Deck
 */

?>

<?php if (ICL_LANGUAGE_CODE) {
    if( ICL_LANGUAGE_CODE == 'es' ) {
        $copyLabel = 'Turn It Around!';
    } else {
        $copyLabel = 'Turn It Around!';
    }
} else {
    $copyLabel = 'Turn It Around!';
} ?>

<footer class="footer">
	<div class="footer__left">
        &copy; <?= Date("Y"); ?> <?= $copyLabel; ?>
    </div>
    <div class="footer__right">
        <?php wp_nav_menu(array(
              'theme_location' => 'footer_menu',
              'menu_class' => 'footer-nav',
          )); ?>
    </div>

</footer>

<!-- PhotoSwipe Markup -->

<script src="https://unpkg.com/scrollreveal"></script>
<script src="<?= get_template_directory_uri(); ?>/assets/js/jquery-3.5.1.min.js"></script>
<!--

<script>
	var slideUp = {
	    interval: 100,
	    delay: 100,
	    duration: 1000,
	    distance: '40px',
	    origin: 'bottom',
	};
	ScrollReveal().reveal('.grid-card', slideUp);
</script>

-->
<?php wp_footer(); ?>


</body>
</html>
