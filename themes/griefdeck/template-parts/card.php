<?php if (ICL_LANGUAGE_CODE) {
    if( ICL_LANGUAGE_CODE == 'es' ) {
        $cardBtnText = 'Haga clic para continuar';
        $btnClass = 'lg';
    } else if( ICL_LANGUAGE_CODE == 'fr' ) {
        $cardBtnText = 'Cliquez pour continuer';
        $btnClass = 'lg';
    } else {
        $cardBtnText = 'Click to Continue';
        $btnClass = '';
    }
} else {
    $cardBtnText = 'Click to Continue';
    $btnClass = '';
} 

$cat = get_the_category();
$catID = $cat[0]->term_id;
$catColor = get_field('color', $cat[0]); ?>

<div class="grid__item">
    <div class="grid-card">
        <div class="grid-card__inner">
            <div class="grid-card__inner--front">
                <!-- <span class="grid-card__color-strip" style="background-color: <?= $catColor; ?>"></span> -->
                <?= get_the_post_thumbnail( $post_id, 'full' ); ?>
            </div>

            <div class="grid-card__inner--back">
                <div class="grid-card__content--back" style="background-color: <?= $catColor; ?>">
                    <div class="grid-card__content--wrapper">
                        <div>
                            <h2 class="grid-card__headline"><?php the_title(); ?></h2>
                            <div class="grid-card__prompt">
                            
                                <?php $promptSummary = get_field('card_summary'); ?>
                                <?php if ($promptSummary) : ?>
                                    <p><?= $promptSummary; ?></p>
                                <?php else : ?>
                                    <?php 

                                    $contentSample = wpautop(get_the_content()); 
                                    $contentSample = strip_tags($contentSample);
                                    $cardSummary = substr($contentSample, 0, 250); 
                                    if (rtrim($cardSummary) != $cardSummary) : ?>
                                        <p><?= rtrim($cardSummary); ?>...</p>
                                    <?php else : ?>
                                        <p><?= $cardSummary; ?>...</p>
                                    <?php endif; ?>
                                <?php endif; ?>
                                
                                <a class="btn <?= $btnClass; ?>" href="<?php the_permalink(); ?>"><?= $cardBtnText; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>