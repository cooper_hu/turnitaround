<?php 
if (ICL_LANGUAGE_CODE) {
    if( ICL_LANGUAGE_CODE == 'es' ) {
        $cardBtnText = 'Aprender más';
    } else if( ICL_LANGUAGE_CODE == 'fr' ) {
        $cardBtnText = 'Apprendre encore plus';
    } else {
        $cardBtnText = 'Learn More';
    }
} else {
    $cardBtnText = 'Learn More';
}

$frontpage_id = get_option( 'page_on_front' );
$includeGuideCard = get_field('include_guide_card', $frontpage_id); ?>

<?php $guideContentFront = get_field('guide_card_front', $frontpage_id);
      $guideContentBack  = get_field('guide_card_back', $frontpage_id); ?>
<div class="grid__item">
    <div class="grid-card">
        <div class="grid-card__inner">
            <div class="grid-card__inner--front green">
                <h1 class="grid-card__headline"><?= $guideContentFront["headline"]; ?></h1>
            </div>

            <div class="grid-card__inner--back green">
                <div class="grid-card__content--back special">
                    <div>
                        <h2 class="grid-card__headline"><?= $guideContentBack["headline"]; ?></h2>
                        <div class="grid-card__prompt">
                            <?= $guideContentBack["copy"]; ?>
                            <?php if ($guideContentBack['link']) : ?> 
                                <a class="btn" href="<?= $guideContentBack['link']; ?>"><?= $cardBtnText; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>