<?php
/**
 * Template Name: Split Page
 * The template for displaying About
 
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
    ATTN: This markup mirrors the resources page template 
 * - - - - - - - - - - - - - - - - - - - - - - - - - -

 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); 

$pageTheme = get_field('theme_color');

?>
    <main class="<?= $pageTheme; ?>">
        <?php while ( have_posts() ) : the_post(); ?>
            <article class="split-page">
                <section class="split-page__left">
                    <div class="container">
                        <h2 class="page__headline"><?php the_title(); ?></h2>
                        <div class="split-page__content">
                            <?php the_field('split_content'); ?>
                        </div>
                    </div>
                </section>
                <?php $image = get_field('split_image'); ?>
                <section class="split-page__right">
                    
                    <img src="<?= $image['url']; ?>"  alt="<?= $image['alt']; ?>"/>
                </section>
            </article>
        <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
<?php get_footer();
