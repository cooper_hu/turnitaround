<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Grief_Deck
 */

get_header();
?>

	<main>
		<section class="page__section">
            <div class="container">
                <h2 class="page__headline">404</h2>
                <div class="page__text center no-border">
                	<a href="/" class="btn">Return Home</a>
             
                </div>
            </div>
        </section>
	</main><!-- #main -->

<?php
get_footer();
