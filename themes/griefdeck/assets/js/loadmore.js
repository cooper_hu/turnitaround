jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error
    // console.log('Exluded IDs', postsNotIn);
    // var postsAreRandom = $('#loadMoreBtn').data('israndom');

    // Get initial postsNotIn
    var postsNotIn = $('#loadMoreBtn').data('excluded-posts');
    var totalPosts = $('#loadMoreBtn').data('total-posts');

    var catID;

    $('#loadMoreBtn').on('click', function(e){
        e.preventDefault();

        console.log('postsNotIn', postsNotIn);

        var button = $(this),
            data = {};

        var lang = button.data('lang');
        var searchTerm = button.data('searchterm');
        var tagArchiveID = button.data('tagid');

        data = {
            'action': 'loadmore',
            'excludedRandPosts': postsNotIn,
        };

        if (catID) {
            data.catID = catID;
        }

        if (lang === 'es') {
            data.lang = 'es';
        } else if (lang === 'fr') {
            data.lang = 'fr';
        }

        if (searchTerm) {
            data.searchTerm = searchTerm;
        }

        if (tagArchiveID) {
            data.tagID = tagArchiveID;
        }

        $.ajax({ // you can also use $.post here
            url : misha_loadmore_params.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                button.find('span').text('Loading...'); // change the button text, you can also add a preloader image
                console.log('Before Arr', data);
            },
            success : function( data ){
                if( data ) { 
                    if (data === 'NOPOST') {
                        // button.parent().remove();
                    } else {
                        $('.grid__container').append(data);

                        var slideUp = {
                            interval: 100,
                            delay: 100,
                            duration: 1000,
                            distance: '40px',
                            origin: 'bottom',
                            reset: false,
                        };

                        ScrollReveal().reveal('.grid-card', slideUp);

                        
                            
                        var newCardIDs = $('#newCardIDs').data('excluded-posts');
                        
                        var mergedArr = $.merge(postsNotIn, newCardIDs);

                        $('#newCardIDs').remove();
                        
                        console.log('totalposts', totalPosts);

                        if ($('.grid__item').length >= totalPosts) {
                            button.parent().addClass('hidden'); 
                        }
                    }
                } else {
                    console.log('error', data);
                }

                button.data('requestRunning', false);
            },
            error: function(data) {
                console.log('Error', data);
            }
        });
    });

    $('.cat-nav__item').on('click', function(e) {
        e.preventDefault();

        postsNotIn = [];

        // console.log('posts not in 1', postsNotIn);

        var catTitle = $(this).html();
        var catColor = $(this).data('color');

        /* *********************
         * REMOVES POPUP STYLING 
         * ********************* */
        var colorWhite = '#f9f8ef';
        var colorGreen = '#2bb673';

        $('.cat-nav__item').each(function() {
            var bgColor = $(this).data('color');
            $(this).css('color', bgColor);
        });

        $('.cat-modal .mfp-close').css('color', colorGreen);
        $('.mfp-bg.cat-modal').css('background', colorWhite);

        $.magnificPopup.close();
        /* ******************
         * REMOVES STYLING 
         * ****************** */
        /* ******************
         * UPDATE HEADER
         * ****************** */

        /* ******************
         * UPDATE HEADER
         * ****************** */



        var button = $(this),
            data = {};

        // Category ID 
        
        catID = $(this).data('catid');
        console.log('catID', catID);

        data = {
            'action': 'loadmore',
            'catID': catID,
            'isFirst': true,
        };
 
        $.ajax({ // you can also use $.post here
            url : misha_loadmore_params.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                console.log('data', data);

                disableScroll();
                $('.grid__container').html('');
                
                $('.grid-cat-header').html(catTitle);

                $('.grid__header--toggle').css('display', 'none');
                $('.grid__header--cat').addClass('visible');

                $('.grid-cat-header').css('color', catColor);

                $('.loading-screen').addClass('visible');
            },
            success : function( data ){
                // console.log(data);

                if( data ) { 
                    if (data === 'NOPOST') {
                        // button.parent().remove();
                    } else {

                        $('.loading-screen').removeClass('visible');
                        $('.grid__container').append(data);

                        enableScroll();

                        var slideUp = {
                            interval: 100,
                            delay: 100,
                            duration: 1000,
                            distance: '40px',
                            origin: 'bottom',
                            reset: false,
                        };

                        ScrollReveal().reveal('.grid-card', slideUp);

                        var newCardIDs = $('#newCardIDs').data('excluded-posts');
                        var mergedArr = $.merge(postsNotIn, newCardIDs);

                        totalPosts = $('#newCardIDs').data('total-posts');

                        $('#newCardIDs').remove();

                        var postPerPage = $('.grid__item').length;
                        
                        if (postPerPage <= totalPosts) {
                            $('.grid__load-more').removeClass('hidden'); 
                        } else if (!$('.grid__load-more').hasClass('hidden')) {
                            $('.grid__load-more').addClass('hidden'); 
                        }
                    }
                } else {
                    console.log('error', data);
                }

                // button.data('requestRunning', false);
            },
            error: function(data) {
                console.log('Error', data);
            }
        });
    });

    $('.js-clear-cat').on('click', function(e) {
        e.preventDefault();

        postsNotIn = [];
        catID = null;

        $('.grid__container').html('');

        $('.grid__header--toggle').css('display', 'block');
        $('.grid__header--cat').removeClass('visible');

        $('.loading-screen').addClass('visible');

        $('.grid__load-more').removeClass('hidden');

        data = {
            'action': 'loadmore',
            'isFirst': true,
        };

        $.ajax({ // you can also use $.post here
            url : misha_loadmore_params.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                disableScroll();
                console.log('Before Arr', data);
            },
            success : function( data ){
                // console.log(data);

                if( data ) { 
                    if (data === 'NOPOST') {
                        // button.parent().remove();
                    } else {
                        $('.loading-screen').removeClass('visible');
                        $('.grid__container').append(data);

                        enableScroll();

                        var slideUp = {
                            interval: 100,
                            delay: 100,
                            duration: 1000,
                            distance: '40px',
                            origin: 'bottom',
                            reset: false,
                        };

                        ScrollReveal().reveal('.grid-card', slideUp);

                        var newCardIDs = $('#newCardIDs').data('excluded-posts');
                        var mergedArr = $.merge(postsNotIn, newCardIDs);

                        totalPosts = $('.grid__container').data('og-total-posts');

                        // $('#newCardIDs').remove();

                        var postPerPage = $('.grid__item').length;

                        console.log('newcardsid', newCardIDs);
                        console.log('totalposts', totalPosts);
                        
                        console.log('postperpage', postPerPage);

                        
                        
                        if (postPerPage < totalPosts) {
                            $('.grid__load-more').removeClass('hidden'); 
                        } else if (!$('.grid__load-more').hasClass('hidden')) {
                            $('.grid__load-more').addClass('hidden'); 
                        }
                    }
                } else {
                    console.log('error', data);
                }

                // button.data('requestRunning', false);
            },
            error: function(data) {
                console.log('Error', data);
            }
        });
    });

    // ON CLICK ACTION IN js/loadmore.js

    /* Enable/Disable scroll based off if the nav drawer is open */
    function disableScroll() { 
        // Get the current page scroll position 
        scrollTop = window.pageYOffset || document.documentElement.scrollTop; 
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft, 
      
            // if any scroll is attempted, set this to the previous value 
            window.onscroll = function() { 
                window.scrollTo(scrollLeft, scrollTop); 
            }; 
    } 
      
    function enableScroll() { 
        window.onscroll = function() {}; 
    } 

});