<?php
/**
 * Template Name: Splash
 * The template for displaying About
 
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
    ATTN: This markup mirrors the resources page template 
 * - - - - - - - - - - - - - - - - - - - - - - - - - -

 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); ?>
    <div class="splash">
        <h1>Turn It Around Project</h1>
        <h2>Coming Soon</h2>
    </div>
<?php get_footer();
