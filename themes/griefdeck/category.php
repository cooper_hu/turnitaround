<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header();

$postsPerPage = get_option( 'posts_per_page' );
$category = get_queried_object();
$catID = $category->term_id;

$args = array(
    'post_type' => 'card',
    'posts_per_page' => $postsPerPage,
    'cat' => $catID
);

if (ICL_LANGUAGE_CODE) {
    if( ICL_LANGUAGE_CODE == 'es' ) {
        $loadBtnText = 'Cargar más tarjetas';
    } else if( ICL_LANGUAGE_CODE == 'fr' ) {
        $loadBtnText = 'Charger plus de cartes';
    } else {
        $loadBtnText = 'Load More Cards';
    }
} else {
    $loadBtnText = 'Load More Cards';
}

$the_query = new WP_Query( $args ); ?>
 
<?php if ( $the_query->have_posts() ) : ?>
    <main>
        <article>
            <div class="grid__container">
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php $post_ids[] = $post->ID; // Build array of post IDs ?>
                    <?php include get_template_directory() . '/template-parts/card.php'; ?>
                <?php endwhile; ?>
            </div>

            <?php if ( $the_query->found_posts >  $postsPerPage ) : ?>
                <div class="container">
                    <div class="grid__load-more">
                        <a href="#" class="btn" id="loadMoreBtn" data-catid="<?= $catID; ?>" data-total-posts="<?= $the_query->found_posts; ?>" data-excluded-posts="<?php echo json_encode($post_ids); ?>" <?php if ($orderOfArchive == 'random') { echo 'data-israndom="true"'; } ?>><?= $loadBtnText; ?></a>
                    </div>
                </div>
            <?php endif; ?>
        </article>
    </main>
<?php endif; ?>

<?php get_footer();
