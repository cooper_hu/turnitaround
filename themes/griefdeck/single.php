<?php
/**
* The template for displaying all single posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Grief_Deck
*/

get_header(); ?>
<main>
    <?php if (ICL_LANGUAGE_CODE) {
        if( ICL_LANGUAGE_CODE == 'es' ) {
            $tagText = 'Etiquetas: ';
            $promptText = 'Apunte de: ';
            $artworkText = 'Obra de arte de: ';
        } else if( ICL_LANGUAGE_CODE == 'fr' ) {
            $tagText = 'Étiquettes: ';
            $promptText = 'Sujet de: ';
            $artworkText = 'Oeuvre de: ';
        } else {
            $tagText = 'Tags: ';
            $promptText = 'Prompt by: ';
            $artworkText = 'Artwork by: ';
        }
    } else {
        $tagText = 'Tags: ';
        $promptText = 'Prompt by: ';
        $artworkText = 'Artwork by: ';
    } ?>

	<?php
	while ( have_posts() ) : the_post(); ?>
		<?php $artistName     = get_field('imgartist', get_post_thumbnail_id()); 
              $artistUrl      = get_field('imgartist_url', get_post_thumbnail_id()); 
              $artistAge      = get_field('imgartist_age', get_post_thumbnail_id()); 
              $artistLocation = get_field('imgartist_location', get_post_thumbnail_id()); 


			  $promptAuthor = get_field('card_prompt_author');
              $promptAuthorUrl = get_field('card_prompt_author_url');
              $promptAuthorAge = get_field('card_prompt_author_age');
              $promptAuthorLocation = get_field('card_prompt_author_location');
			  $printEdition = get_field('card_print_edition'); ?>

		<article id="post-<?php the_ID(); ?>" class="prompt">
			<section class="prompt__image">
                <?php $horizontalCard = get_field('horizontal_card'); ?>
				<div class="prompt__card--wrapper <?php echo ($horizontalCard) ? 'horizontal' : 'vertical'; ?>">
					<div class="prompt__card <?php echo ($horizontalCard) ? 'horizontal' : 'vertical'; ?>" id="singleCard">
                        <?php if ($horizontalCard) : 
                            $horizontalImage = get_field('horizontal_image'); ?>
                            <div id="horizontalImage" style="display: none;" data-url="<?= $horizontalImage['url']; ?>" data-width="<?= $horizontalImage['width']; ?>"  data-height="<?= $horizontalImage['height']; ?>"></div>
                            <img src="<?= $horizontalImage['url']; ?>" alt="<?= $horizontalImage['alt']; ?>" width="<?= $horizontalImage['width']; ?>" height="<?= $horizontalImage['height']; ?>"/>
                        <?php else: ?>
                            <!-- <img src="<?php echo get_the_post_thumbnail_url(); ?>"/>-->
						  <?php griefdeck_post_thumbnail(); ?>
                        <?php endif; ?>
					</div>
				</div>

				<div class="prompt__image--caption">
                    <div class="arrow prev">
                        <?php next_post_link('%link', 'Next: %title'); ?>
                    </div>
                    <!-- <a href="#" class="prompt__image--nav-arrows prev"></a> -->
                    <div class="text">
    					<?php if ($artistName) : ?>
                            <?php if ($artistUrl) : ?>
                            
    						  <p><?= $artworkText; ?><a href="<?= $artistUrl; ?>" target="_blank"><?= $artistName; ?></a><?php if ($artistAge) : ?><span class="age">, <?= $artistAge; ?></span><?php endif; ?><?php if ($artistLocation) : ?>. <span class="location"><?= $artistLocation; ?><?php endif; ?></p>
                            <?php else : ?>
                                
                                <p><?= $artworkText; ?><?= $artistName; ?><?php if ($artistAge) : ?><span class="age">, <?= $artistAge; ?></span><?php endif; ?><?php if ($artistLocation) : ?>. <span class="location"><?= $artistLocation; ?><?php endif; ?></p>
                            <?php endif; ?>
    					<?php endif;?>
                    </div>
                    <div class="arrow next">
                        <?php previous_post_link('%link', 'Previous: %title'); ?>
                    </div>
                    <!--
                    <a href="#" class="prompt__image--nav-arrows next"></a>
                    -->
				</div>
			</section>
			<section class="prompt__content">
				<div class="prompt__content--container">
                    <div class="top"> <?php // wrapped in a div so justify-content: space-between works on desktop layout ?>
					   <?php the_title( '<h1 class="prompt__title">', '</h1>' ); ?>
                    
                        <div class="prompt__textarea">
    						<?php
    						the_content(
    							sprintf(
    								wp_kses(
    									/* translators: %s: Name of current post. Only visible to screen readers */
    									__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'griefdeck' ),
    									array(
    										'span' => array(
    											'class' => array(),
    										),
    									)
    								),
    								wp_kses_post( get_the_title() )
    							)
    						); ?>
    						
    					</div><!-- .prompt__textarea -->
                    </div>

                    <div class="prompt__meta">
                            <div class="prompt__meta--left">
                                <?php if ($promptAuthor) : ?>
                                    <?php if ($promptAuthorUrl) : ?>
                                      
                                        <span class="prompt-by"><?= $promptText; ?><a href="<?= $promptAuthorUrl; ?>" target="_blank"><?= $promptAuthor; ?></a><?php if ($promptAuthorAge) : ?><span class="age">, <?= $promptAuthorAge; ?></span><?php endif; ?><?php if ($promptAuthorLocation) : ?>. <span class="location"><?= $promptAuthorLocation; ?><?php endif; ?>
                                        </span>
                                    <?php else : ?>
                               
                                        <span class="prompt-by"><?= $promptText; ?><?= $promptAuthor; ?><?php if ($promptAuthorAge) : ?><span class="age">, <?= $promptAuthorAge; ?></span><?php endif; ?><?php if ($promptAuthorLocation) : ?>. <span class="location"><?= $promptAuthorLocation; ?><?php endif; ?>
                                    <?php endif; ?>
                                <?php endif;?>
                            </div>
                            <div class="prompt__meta--right">
                                <div class="prompt__tags">
                                    <?php the_tags( $tagText, ', ', '' ); ?>
                                </div>
                            </div>
                        </div>
				</div>
			</section>
		</article>
	<?php endwhile; ?>
</main>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <!--  Controls are self-explanatory. Order can be changed. -->
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
                    
<?php 
/* 
echo count(get_the_content());

if (get_the_content() >= 255) {
the_content();
} else {
$str = wpautop(get_the_content());
$str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );


echo $str;
//  $str = strip_tags($str, '<a><strong><em>');
// return '<p>' . $str . '</p>';
} ?>
<p>IF STATEMENT ABOVE</p>
<?php 
$prompt = wpautop(get_the_content());
var_dump($prompt);
?>
<br/><br/><br/><br/>
<?php 
$content = get_the_content( 'Read more' );

echo wpautop(get_the_content()); */ ?>

<?php get_footer();
