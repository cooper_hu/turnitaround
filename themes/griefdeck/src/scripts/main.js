/* **************************
 * Sitewide: Nav
 * ************************** */
$('.navbar__toggle').on('click', function(e) {
    disableScroll();
    $('.navbar-drawer').addClass('visible');
    $('.navbar-overlay').fadeIn();

});

$('.navbar-overlay').on('click', function(e) {
    enableScroll();
    $('.navbar-overlay').fadeOut();
    $('.navbar-drawer').removeClass('visible');
});

$('.navbar-drawer__close-btn').on('click', function(e) {
    enableScroll();
    $('.navbar-overlay').fadeOut();
    $('.navbar-drawer').removeClass('visible');
});

$('.navbar-list .wpml-ls-item').on('mouseover', function(e) {
  console.log('hover');
  $(this).find('.sub-menu').addClass('active');
});

$('.navbar-list .wpml-ls-item').on('mouseleave', function(e) {
  console.log('hover');
  $(this).find('.sub-menu').removeClass('active');
});

// If the Nav is open and someone pressed the escape key close it
$(document).ready(function(){
    $(document).bind('keydown', function(e) { 
        if (e.which === 27 && $('.navbar-drawer').hasClass('visible')) {
            enableScroll();
            $('.navbar-overlay').fadeOut();
            $('.navbar-drawer').removeClass('visible');
        }
    }); 
});

$(window).resize(function() {
    if ($(window).width() >= 1000) {
        enableScroll();
    } else {
        if ($('.navbar-drawer').hasClass('visible')) {
            disableScroll();
        }
    }
});

$('.navbar__search-form--btn').on('click', function(e) {
    e.preventDefault();
    $('.navbar__search-form').toggleClass('visible');
}); 

// Open Homepage Toggle
$('.js-open-cat-modal').magnificPopup({
  type: 'inline',

  mainClass: 'cat-modal mfp-fade',
  closeOnBgClick: false,
  closeBtnInside: false,
  fixedContentPos: true,

  // other options
});

var colorWhite = '#f9f8ef';
var colorGreen = '#2bb673';

// CAT HOVER
$('.cat-nav__item').on('mouseover', function(e) {
  var bgColor = $(this).data('color');

  $('.cat-nav__item').css('color', bgColor);
  $(this).css('color', colorWhite);
  $('.cat-modal .mfp-close').css('color', colorWhite);
  $('.mfp-bg.cat-modal').css('background', bgColor);
});

$('.cat-nav__item').on('mouseleave', function(e) {

  $('.cat-nav__item').each(function() {
    var bgColor = $(this).data('color');
    $(this).css('color', bgColor);
  });
  
  $('.cat-modal .mfp-close').css('color', colorGreen);
  $('.mfp-bg.cat-modal').css('background', colorWhite);
});

// ON CLICK ACTION IN js/loadmore.js


/* Enable/Disable scroll based off if the nav drawer is open */
function disableScroll() { 
    // Get the current page scroll position 
    scrollTop = window.pageYOffset || document.documentElement.scrollTop; 
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft, 
  
        // if any scroll is attempted, set this to the previous value 
        window.onscroll = function() { 
            window.scrollTo(scrollLeft, scrollTop); 
        }; 
} 
  
function enableScroll() { 
    window.onscroll = function() {}; 
} 

/* **************************
 * Single: Card. Expand Image
 * ************************** */
var items = [];

var openPhotoSwipe = function() {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // define options (if needed)
    var options = {
             // history & focus options are disabled on CodePen        
        history: false,
        focus: false,
        bgOpacity: 0.95,
        shareEl: false,
        fullscreenEl: false,
        // showAnimationDuration: 250,
        // hideAnimationDuration: 250
    };
    
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};

if ($('.prompt__card')[0]) {
    $('.prompt__card').each(function() {

      if ($(this).find('#horizontalImage')[0]) {
        items.push({
          src: $(this).find('#horizontalImage').data('url'),
          w:   $(this).find('#horizontalImage').data('width'),
          h:   $(this).find('#horizontalImage').data('height')
        })
      } else {
        items.push({
            src: $(this).find('img').attr('src'),
            w:   $(this).find('img').attr('width'),
            h:   $(this).find('img').attr('height')
        });
      }
    });

    // console.log('Item', items);

    document.getElementById('singleCard').onclick = openPhotoSwipe;
}

/* **************************
 * Submission Form
 * ************************** */

//Reference: 
//https://www.onextrapixel.com/2012/12/10/how-to-create-a-custom-file-input-with-jquery-css3-and-php/
;(function($) {

  // Browser supports HTML5 multiple file?
  var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
      isIE = /msie/i.test( navigator.userAgent );

  $.fn.customFile = function() {

    return this.each(function() {

      if ($('html').attr('lang') === 'es-ES') {
        var buttonLabel = 'Subir arte';
      } else if ($('html').attr('lang') === 'fr-FR') {
        var buttonLabel = "Télécharger l'art";
      } else {
        var buttonLabel = 'Upload Art';
      }

      var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
          $wrap = $('<div class="file-upload-wrapper">'),
          $button = $('<button type="button" class="file-upload-button">' + buttonLabel + '</button>'),
          $input = $('<input type="text" class="file-upload-input" />'),
          // Button that will be used in non-IE browsers
          // Hack for IE
          $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">' + buttonLabel + '</label>');

      // Hide by shifting to the left so we
      // can still trigger events
      $file.css({
        position: 'absolute',
        left: '-9999px'
      });

      $wrap.insertAfter( $file )
        .append( $file, ( isIE ? $label : $button ), $input );

      // Prevent focus
      $file.attr('tabIndex', -1);
      $button.attr('tabIndex', -1);

      $button.click(function () {
        $file.focus().click(); // Open dialog
      });

      $file.change(function() {

        var files = [], fileArr, filename;

        // If multiple is supported then extract
        // all filenames from the file array
        if ( multipleSupport ) {
          fileArr = $file[0].files;
          for ( var i = 0, len = fileArr.length; i < len; i++ ) {
            files.push( fileArr[i].name );
          }
          filename = files.join(', ');

        // If not supported then just take the value
        // and remove the path to just show the filename
        } else {
          filename = $file.val().split('\\').pop();
        }

        $input.val( filename ) // Set the value
          .attr('title', filename) // Show filename in title tootlip
          .focus(); // Regain focus

      });

      $input.on({
        blur: function() { $file.trigger('blur'); },
        keydown: function( e ) {
          if ( e.which === 13 ) { // Enter
            if ( !isIE ) { $file.trigger('click'); }
          } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
            // On some browsers the value is read-only
            // with this trick we remove the old input and add
            // a clean clone with all the original events attached
            $file.replaceWith( $file = $file.clone( true ) );
            $file.trigger('change');
            $input.val('');
          } else if ( e.which === 9 ){ // TAB
            return;
          } else { // All other keys
            return false;
          }
        }
      });

    });

  };

  // Old browser fallback
  if ( !multipleSupport ) {
    $( document ).on('change', 'input.customfile', function() {

      var $this = $(this),
          // Create a unique ID so we
          // can attach the label to the input
          uniqId = 'customfile_'+ (new Date()).getTime(),
          $wrap = $this.parent(),

          // Filter empty input
          $inputs = $wrap.siblings().find('.file-upload-input')
            .filter(function(){ return !this.value }),

          $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

      // 1ms timeout so it runs after all other events
      // that modify the value have triggered
      setTimeout(function() {
        // Add a new input
        if ( $this.val() ) {
          // Check for empty fields to prevent
          // creating new inputs when changing files
          if ( !$inputs.length ) {
            $wrap.after( $file );
            $file.customFile();
          }
        // Remove and reorganize inputs
        } else {
          $inputs.parent().remove();
          // Move the input so it's always last on the list
          $wrap.appendTo( $wrap.parent() );
          $wrap.find('input').focus();
        }
      }, 1);

    });
  }

}(jQuery));

$('input[type=file]').customFile();

// When submit is pressed
$( '.wpcf7-submit' ).click(function() {
  // Clicked!
  console.log('Clicked!');
  $(this).css('display', 'none');
  // $(this).attr( 'disabled', true );
});

if ($('body').hasClass('page-template-template-submission')) {
  $('select').selectric();
}


// If form is invalid
document.addEventListener( 'wpcf7invalid', function( event ) {
  $('.wpcf7-submit').css('display', 'inline-block');
  // console.log('Invalid!!');
}, false );

// On Submit function
document.addEventListener( 'wpcf7submit', function( event ) {
  console.log('Submit!!');

  if ($('html').attr('lang') === 'es-ES') {
    var errorLabel = 'El campo es obligatorio.';
  } else {
    var errorLabel = 'The field is required.';
  }
    
    if ($('.form-submit__confirm').find('span.wpcf7-not-valid-tip')[0]) {
      // console.log('NEED TO FILL');
      $('.form-submit__confirm').append('<span class="wpcf7-not-valid-tip" aria-hidden="true">' + errorLabel + '</span>');
    }
}, false );



/* **************************
 * Cards
 * ************************** */
var slideUp = {
      interval: 100,
      delay: 100,
      duration: 1000,
      distance: '40px',
      origin: 'bottom',
      reset: false,
};

ScrollReveal().reveal('.grid-card', slideUp);