<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); ?>
    <main>
        <?php if ( have_posts() ) : ?>
            <article>
                <div class="grid__container">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="grid__item">
                            <div class="grid-card">
                                <div class="grid-card__inner">
                                    <div class="grid-card__inner--front">
                                        <?= get_the_post_thumbnail( $post_id, 'full' ); ?>
                                    </div>

                                    <div class="grid-card__inner--back">
                                        <div class="grid-card__content--back">
                                            <h2 class="grid-card__headline"><?php the_title(); ?></h2>
                                            <div>
                                                <?php $promptSummary = get_field('card_summary'); ?>
                                                <?php if ($promptSummary) : ?>
                                                    <p><?= $promptSummary; ?></p>
                                                <?php else : ?>
                                                    <?php $contentSample = strip_tags(get_the_content()); 
                                                    $cardSummary = substr($contentSample, 0, 270); 
                                                    if (rtrim($cardSummary) != $cardSummary) : ?>
                                                        <p><?= rtrim($cardSummary); ?>...</p>
                                                    <?php else : ?>
                                                        <p><?= $cardSummary; ?>...</p>
                                                    <?php endif; ?>

                                                <?php endif; ?>
                                                <a href="<?php the_permalink(); ?>">Click to Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile;
                    the_posts_navigation(); ?>
                
                </div>
            </article>
        <?php endif; ?>
    </main><!-- #main -->
<?php get_footer();
