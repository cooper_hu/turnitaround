<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); ?>

<main>
    <article>
        <section class="page__section">
            <div class="container">
                <h1 class="page__headline"><?php the_title(); ?></h1>
                <div class="page__text">
                    <?php the_content(); ?>
                </div>
        </section>

    	<?php /* Page Module code (unused)
        <?php if( have_rows('sections') ) : ?>
            <article>
                <?php while ( have_rows('sections') ) : the_row(); ?>
                    <section class="page__section">
                        <?php the_content(); ?>
                    </section>
                    
                    <?php if( get_row_layout() == 'mod_text' ) :
                        $color = get_sub_field('mod_text_bgcolor');
                        $headline = get_sub_field('mod_text_headline');
                        $text = get_sub_field('mod_text_text'); ?>
                        <section class="page__section <?= $color; ?>">
                            <div class="container">
                                <h2 class="page__headline"><?= $headline; ?></h2>

                                <div class="page__text">
                                    <?= $text; ?>
                                </div>
                            </div>
                        </section>
                    <?php elseif( get_row_layout() == 'mod_imgtxt' ) : 
                        $color = get_sub_field('mod_imgtxt_bgcolor');
                        $headline = get_sub_field('mod_imgtxt_headline');
                        $text = get_sub_field('mod_imgtxt_text'); ?>
                        <section class="page__section <?= $color; ?>">

                        </section>
                    <?php endif; ?>
                    
                <?php endwhile; ?>
            </article>
        <?php endif; ?>
        */ ?>
    </article>
</main><!-- #main -->

<?php get_footer();
