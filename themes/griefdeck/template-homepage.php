<?php
/**
 * Template Name: Homepage
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); ?>
    <main>
        <?php 
        $orderOfArchive = get_field('homepage_order');
        $includeGuideCard = get_field('include_guide_card'); 
        $ppp = get_option( 'posts_per_page' );

        if ($includeGuideCard) {
            $postsPerPage = $ppp - 1;
        } else {
            $postsPerPage = $ppp;
        }

        $args = array(
            'post_type' => 'card',
            'posts_per_page' => $postsPerPage,
        ); 

        if ($orderOfArchive == 'random') {
            $args['orderby'] = 'rand';
        }

        

        if (ICL_LANGUAGE_CODE) {
            if( ICL_LANGUAGE_CODE == 'es' ) {
                // $cardBtnText = 'Aprender más';
                $loadBtnText = 'Cargar más tarjetas';
            } else if( ICL_LANGUAGE_CODE == 'fr' ) {
                // $cardBtnText = 'Apprendre encore plus';
                $loadBtnText = 'Charger plus de cartes';
            } else {
                // $cardBtnText = 'Learn More';
                $loadBtnText = 'Load More Cards';
            }
        } else {
            // $cardBtnText = 'Learn More';
            $loadBtnText = 'Load More Cards';
        }

        $the_query = new WP_Query( $args ); ?>
 
        <?php if ( $the_query->have_posts() ) : ?>
            <?php // echo '<pre>'; var_dump($the_query["posts"]); echo '</pre>'; ?>
            <article>
                <div class="grid__header">
                    <div class="dropdown-container">
                        <div class="grid__header--toggle">
                            <a href="#categoryModal" class="btn-grid-sort js-open-cat-modal" id="btnOpen">How do we turn it around?</a>
                        </div>
                        <div class="grid__header--cat">
                            <h3 class="grid-cat-header"></h3>
                            <a href="#categoryModal" class="btn-grid-clear js-open-cat-modal" id="btnOpen">More Categories</a>
                            <a class="btn-grid-clear extra-space js-clear-cat">Clear</a>
                        </div>
                    </div>
                </div>
                <div class="grid__container" <?php if ($includeGuideCard) { echo 'data-includeguide="true"'; } ?> data-og-total-posts="<?= $the_query->found_posts; ?>">
                    <?php if ($includeGuideCard) : ?>
                        <?php include get_template_directory() . '/template-parts/guide-card.php'; ?>
                    <?php endif; ?>

                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php $post_ids[] = $post->ID; // Build array of post IDs ?>
                        <?php include get_template_directory() . '/template-parts/card.php'; ?>
                    <?php endwhile; ?>
                </div>

                <?php if ( $the_query->found_posts >  $postsPerPage ) : ?>
                    <div class="container">
                        <div class="grid__load-more">
                            <a href="#" class="btn" id="loadMoreBtn" data-total-posts="<?= $the_query->found_posts; ?>" data-excluded-posts="<?php echo json_encode($post_ids); ?>" data-lang="<?php if (ICL_LANGUAGE_CODE) { echo ICL_LANGUAGE_CODE; } ?>"><?= $loadBtnText; ?></a>
                        </div>
                    </div>
                <?php endif; ?>

            </article>
        <?php endif; ?>
    </main><!-- #main -->

    <div class="mfp-hide" id="categoryModal">
        <nav class="cat-nav">
            <?php $cats = get_categories( array(
                'hide_empty' => false,
            ));

            foreach($cats as $cat) :
                $catId = $cat->term_id; 
                $catTitle = $cat->name;
                $catColor = get_field('color', $cat); ?>

                <a class="cat-nav__item" href="#" data-catid="<?= $catId; ?>" data-color="<?= $catColor; ?>" style="color: <?= $catColor; ?>"><?= $catTitle; ?></a>

            <?php endforeach; ?>
        </nav>

        <?php // echo get_cat(); ?>
    </div>

    <div class="loading-screen"></div>
<?php get_footer();
