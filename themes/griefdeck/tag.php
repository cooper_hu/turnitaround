<?php
/**
 * The template for displaying tag archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); ?>

<main>
    <?php 

    if (ICL_LANGUAGE_CODE) {
        if( ICL_LANGUAGE_CODE == 'es' ) {
            $loadBtnText = 'Cargar más tarjetas';
        } else if( ICL_LANGUAGE_CODE == 'fr' ) {
            $loadBtnText = 'Charger plus de cartes';
        } else {
            $loadBtnText = 'Load More Cards';
        }
    } else {
        $loadBtnText = 'Load More Cards';
    }

    // Tag Specific query information
    $tag = get_queried_object();
    $tagID = $tag->term_id;
    
    $ppp = get_option( 'posts_per_page' );
    $postsPerPage = $ppp - 1; // Offset for the tag block

    $args = array(
        'post_type' => 'card',
        'posts_per_page' => $postsPerPage,  
        'tag_id' => $tagID,
    );

    $the_query = new WP_Query( $args ); ?>

    <?php if ( $the_query->have_posts() ) : ?>
        
        <div class="grid__container">
            <div class="grid__item">
                <div class="tag-header">
                    <h1><?php the_archive_title( ); ?></h1>
                </div>
            </div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <?php $post_ids[] = $post->ID; // Build array of post IDs ?>
                <?php include get_template_directory() . '/template-parts/card.php'; ?>
            <?php endwhile; ?>
        </div>

        <?php if ( $the_query->found_posts >  $postsPerPage ) : ?>
            <div class="container">
                <div class="grid__load-more">
                    <a href="#" class="btn" id="loadMoreBtn" data-total-posts="<?= $the_query->found_posts; ?>" data-excluded-posts="<?php echo json_encode($post_ids); ?>" data-tagid="<?= $tagID; ?>"><?= $loadBtnText; ?></a>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

</main>

<?php get_footer();
