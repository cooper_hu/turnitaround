<?php
/**
 * Template Name: Resources
 * The template for displaying Resources Page. 
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
    ATTN: This markup mirrors the split text template 
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Grief_Deck
 */

get_header(); 

$pageTheme = get_field('theme_color');

?>
    <main class="<?= $pageTheme; ?>">
        <?php while ( have_posts() ) : the_post(); ?>
            <article class="resource-page">
                
                <div>
                    <h2 class="page__headline"><?php the_title(); ?></h2>
                    <div>
                        <?php the_field('split_content'); ?>
                    </div>

                    <?php if( have_rows('resources_repeater') ): ?>
                        <div class="split-page__content collapsed">
                            <p><?php the_field('list_headline'); ?></p>
                           
                            <?php while( have_rows('resources_repeater') ) : the_row(); 
                                $resourceImage = get_sub_field('resource_image');
                                $resourceLink = get_sub_field('resource_link'); ?>
                                <div class="resource-item">
                                    <div class="resource-item__left">
                                        <?php if ($resourceImage) : ?>
                                            <?php if ($resourceLink) : ?>
                                                <a href="<?= $resourceLink; ?>" target="_blank">
                                                    <img src="<?= $resourceImage['url']; ?>" alt="<?= $resourceImage['alt']; ?>"/>
                                                </a>
                                            <?php else : ?>
                                                <img src="<?= $resourceImage['url']; ?>" alt="<?= $resourceImage['alt']; ?>"/>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="resource-item__right">
                                        <?php if ($resourceLink) : ?>
                                            <a href="<?= $resourceLink; ?>" target="_blank">
                                                <h3><?php the_sub_field('resource_title'); ?></h3>    
                                            </a>
                                        <?php else : ?>
                                            <h3><?php the_sub_field('resource_title'); ?></h3>
                                        <?php endif; ?>
                                        
                                        <?php the_sub_field('resource_description'); ?>
                                    </div>
                                </div>
                              
                            <?php endwhile; ?>
                           
                        </div>
                    <?php endif; ?>
                </div>
                
                
            </article>
        <?php endwhile; // End of the loop. ?>
    </main><!-- #main -->
<?php get_footer();
